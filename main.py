import IoAccess
import time
import schedule
from PeronosporaEvaluator import PeronosporaEvaluator
from datetime import timedelta
from datetime import date
from decouple import config
import traceback
import datetime

date_pattern = "%Y-%m-%d %H:%M:%S"
evaluators_list = []


def init():
    env = config("ENV")
    stations = IoAccess.get_stations()
    for i in range(0, len(stations)):
        print("===================== PROCESSAMENTO STAZIONE " + str(stations[i]['id_stazione']) + " =====================")
        try:
            evaluator = PeronosporaEvaluator(IoAccess.get_weather_data(stations[i]['id_stazione'], date(date.today().year, 1, 1).strftime(date_pattern),
                                                (datetime.datetime(date.today().year, date.today().month, date.today().day, 0, 0, 0) + timedelta(days=5) - timedelta(seconds=1)).strftime(date_pattern)))
            if not evaluator.check_missing_data():
                evaluator.evaluate_daily_leaf_wet()
                evaluator.evaluate_day_degrees()
                evaluator.evaluate_phenologic_phases()
                model_manual_date = stations[i]['data_ricettivita_peronospora']
                # if model_manual_date is not None:
                #     model_manual_date = model_manual_date.split(".")[0].replace("T"," ")
                data = evaluator.evaluate_peronospora(model_manual_date)
                if env == "PROD":
                    IoAccess.delete_data(str(stations[i]['id_stazione']))
                    IoAccess.send_data(data, str(stations[i]['id_stazione']))
        except Exception as e:
            traceback.print_exc()
            pass

# def update_data():
#     stations = IoAccess.get_stations()
#     for i in range(0, len(evaluators_list)):
#         evaluators_list[i].set_dataset(IoAccess.get_weather_data(stations[i]['id_stazione'],
#                                                 (datetime.datetime(date.today().year, date.today().month, date.today().day, 0, 0, 0) - timedelta(days=3)).strftime(date_pattern),
#                                                 (datetime.datetime(date.today().year, date.today().month, date.today().day, 0, 0, 0) + timedelta(days=5) - timedelta(seconds=1)).strftime(date_pattern)))
#         data = evaluators_list[i].evaluate_peronospora()
#         IoAccess.send_data(data)


def main():
    init()


if __name__ == "__main__":
    main()