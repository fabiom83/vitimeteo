import requests as http
from decouple import config


def get_weather_data(station, date_start = None, date_end = None ):
    # type: (int, str) -> json
    get_string = config('API_GET_WEATHER_URL') + "?idStation=" + str(station)
    if date_start is not None:
        get_string = get_string + "&dateStart=" + date_start
    if date_end is not None:
        get_string = get_string + "&dateEnd=" + date_end
    print(get_string)
    r = http.get(get_string , auth=(config('API_USER'), config('API_PWD')))
    if r.status_code == 200:
        print("Download dati eseguito. Tempo di esecuzione: " + str(r.elapsed))
        return r.json()
    else:
        print("Errore download dati: " + str(r.reason))



def get_stations():
    get_string = config('API_GET_STATIONS_URL') + "?projectID=" + config('PROJECT_ID')
    r = http.get(get_string, auth=(config('API_USER'), config('API_PWD')))
    return r.json()


def send_data(data, station):
    post_string = config('API_POST_PERONOSPORA_DATA_URL')
    r = http.post(post_string, json=data, auth=(config('API_USER'), config('API_PWD')))
    if r.status_code == 200:
        print("Invio dati eseguito su stazione " + str(station) + ". Tempo di esecuzione: " + str(r.elapsed))
    else:
        print("Errore invio dati: " + str(r.reason))

# cancella i dati precedentemente inseriti, se presenti, relativi al modello manuale 
def delete_data(station):
    delete_string = config('API_DELETE_PERONOSPORA_DATA_URL') + "?station=" + station
    r = http.get(delete_string, auth=(config('API_USER'), config('API_PWD')))
    if r.status_code == 200:
        print("Cancellazione dati eseguita su stazione " + station + ". Tempo di esecuzione: " + str(r.elapsed))
    else:
        print("Errore cancellazione dati: " + str(r.reason))
    