import datetime as date_time
from datetime import datetime
from datetime import timedelta



class PeronosporaEvaluator:

    DATE_PATTERN = "%Y-%m-%d %H:%M:%S"
    STARTING_CONDITION_ZERO_DEVELOP = 8
    STARTING_CONDITION_DAY_DEGREES_THRESHOLD = 150 #gradi giorno condizione iniziale
    PHENOLOGIC_STATE_ZERO_DEVELOP = 10
    PHENOLOGIC_STATE_DAY_DEGREES_THRESHOLD = 120 #gradi giorno stato fenologico
    LW_THRESHOLD_MINUTES = 20
    GERMINATION_TEMPERATURE_AVG_THRESHOLD = 8
    GERMINATION_RAIN_MM_SUM_THRESHOLD = 5
    PRIMARY_INFESTATION_LW_THRESHOLD_PERCENTAGE = 55 #bagnatura fogliare
    PRIMARY_INFESTATION_DAY_DEGREES_THRESHOLD = 50
    PRIMARY_INFESTATION_RAIN_INTENSITY_THRESHOLD = 3
    INCUBATION_THRESHOLD_PERCENTAGE = 70
    SPORULATION_HUMIDITY_THRESHOLD = 92
    SPORULATION_CONSECUTIVE_HOURS = 4
    SPORULATION_TEMPERATURE_THRESHOLD = 12
    SPORULATION_PERSISTENCE_DAYS = 5
    SEC_INFESTATION_DAY_DEGREES_THRESHOLD = 50
    NULLABLE_VALUE = -9999


    EVENT_MATURAZIONE_OOSPORE = "Maturazione oospore"
    EVENT_SVILUPPO_FENOLOGICO_VITE = "Sviluppo fenologico vite"
    EVENT_INFEZIONE_PRIMARIA = "Infezione primaria"
    EVENT_INCUBAZIONE_PRIMARIA = "Incubazione infezione primaria"
    EVENT_SOGLIA_INCUBAZIONE_PRIMARIA_SUPERATA = "Soglia incubazione primaria superata"
    EVENT_SPORULAZIONE = "Sporulazione"
    EVENT_INFEZIONE_SECONDARIA = "Infezione secondaria"
    EVENT_INCUBAZIONE_SECONDARIA = "Incubazione infezione secondaria"
    EVENT_SOGLIA_INCUBAZIONE_SECONDARIA_SUPERATA = "Soglia incubazione secondaria superata"
    EVENT_BAGNATURA_FOGLIARE = "Bagnatura"
    EVENT_GRADI_GIORNO = "Gradi-Giorno"
    EVENT_GRADI_BAGNATURA = "Gradi-Ora"


    #STATI FENOLOGICI
    EVENT_GERMOGLIAMENTO = "Germogliamento"
    EVENT_FIORITURA = "Fioritura"
    EVENT_ALLEGAGIONE = "Allegagione"
    EVENT_INIZIO_INVAIATURA = "Inizio Invaiatura"
    EVENT_INVAIATURA_COMPLETA = "Invaiatura Completa"
    EVENT_MATURAZIONE = "Maturazione"

    GERMOGLIAMENTO_THRESHOLD = 103.66
    FIORITURA_THRESHOLD = 465.66
    ALLEGAGIONE_THRESHOLD = 512
    INIZIO_INVAIATURA_THRESHOLD = 1176.33
    INVAIATURA_COMPLETA_tHRESHOLD = 1349.33
    MATURAZIONE_THRESHOLD = 1790

    is_update = False
    is_manual_model = False
    starting_index = None
    phenologic_condition_index = None
    phenologic_condition_index_manual = None
    is_germination_present = False
    germination_index = None
    is_infestation_present = False
    is_incubation_reached = False
    #primary_infestation_indexes = []
    #incubation_percentage_sum = 0
    #primary_infestation_incubation_indexes = []
    is_secondary_infestation_present = False
    starting_condition_already_checked = False
    phenologic_condition_already_checked = False
    germination_already_checked = False
    infestation_already_checked = False

    # last_known_t_avg = 10 # type: float
    # last_known_t_min = 0 # type: float
    # last_known_lw_top = [40] * 60
    # last_known_lw_bottom = [40] * 60
    # last_known_hum_min = 70 # type: float
    # last_known_rain_tot = 0 # type: float
    # last_known_rain_max = 10  # type: float


    output = {"data": []}

    def __init__(self, dataset):
        self.set_dataset(dataset)
        self.is_update = False

    def set_dataset(self, dataset):
        self.dataset = dataset
        self.is_update = True

    def get_lw_values(self, index):
        return self.dataset[index]['lw_top_array_tot'],  self.dataset[index]['lw_bottom_array_tot']

    def get_t_avg_value(self, index):
        if self.dataset[index]['t_avg'] is not None:
            t_avg = float(self.dataset[index]['t_avg'])
        else:
            t_avg = None
        return t_avg

    def get_t_min_value(self, index):
        if self.dataset[index]['t_min'] is not None:
            t_min = float(self.dataset[index]['t_min'])
        else:
            t_min = None
        return t_min

    def get_hum_min_value(self, index):
        return self.dataset[index]['hum_min']

    def get_rain_max_value(self, index):
        if self.dataset[index]['rain_max'] is not None:
            rain_max = float(self.dataset[index]['rain_max'])
        else:
            rain_max = None
        return rain_max

    def get_rain_tot_value(self, index):
        if self.dataset[index]['rain_tot'] is not None:
            rain_tot = float(self.dataset[index]['rain_tot'])
        else:
            rain_tot = None
        return rain_tot

    def check_missing_data(self):
        i = 0
        if len(self.dataset) == 0:
            is_missing_data = True
            print("No data")
        else:
            is_missing_data = False
            print("Controllo dati mancanti stazione: " + str(self.dataset[0]['id_stazione']))
        while not is_missing_data and i < len(self.dataset):
            lw_top, lw_bot = self.get_lw_values(i)
            if self.get_t_avg_value(i) is None or self.get_t_avg_value(i) == self.NULLABLE_VALUE or \
                    self.get_t_min_value(i) is None or self.get_t_min_value(i) == self.NULLABLE_VALUE or \
                    self.get_hum_min_value(i) is None or self.get_hum_min_value(i) == self.NULLABLE_VALUE or \
                    self.get_rain_max_value(i) is None or self.get_rain_max_value(i) == self.NULLABLE_VALUE or \
                    self.get_rain_tot_value(i) is None or self.get_rain_tot_value(i) == self.NULLABLE_VALUE or \
                    lw_top is None or lw_top == self.NULLABLE_VALUE or \
                    lw_bot is None or lw_bot == self.NULLABLE_VALUE:
                is_missing_data = True
                print("Dati mancanti trovati il giorno: " + self.dataset[i]['date_hour'])
            i = i + 1
        return is_missing_data

    def evaluate_peronospora(self, phenologic_condition_date_manual = None):
        #if not self.check_missing_data():
        id_stazione = self.dataset[0]['id_stazione']
        #sample = {'id_stazione': self.dataset[0]['id_stazione'], 'lw_threshold': self.INFESTATION_LW_THRESHOLD}
        print("Valutazione peronospora stazione: " + str(id_stazione))
        if not self.starting_condition_already_checked:
            self.starting_index = self.__check_starting_condition()
        else:
            print("Maturazione oospore gia rilevata")
        if phenologic_condition_date_manual is not None:
            self.phenologic_condition_index_manual = self.__get_index_by_date(phenologic_condition_date_manual)
            print("----------------- Modello manuale -----------------")
            print("Data sviluppo fenologico vite manuale: " + phenologic_condition_date_manual)
        else:
            print("----------------- Modello calcolato -----------------")
        if not self.phenologic_condition_already_checked:
            self.phenologic_condition_index = self.__check_phenologic_condition()
        else:
            print("Sviluppo fenologico vite gia rilevato")
        # condizioni inziali di maturazione oospore e sviluppo fenologico raggiunte
        if self.starting_index is not None and (self.phenologic_condition_index is not None or self.phenologic_condition_index_manual is not None):
            if not self.starting_condition_already_checked:
                print("Maturazione oospore rilevata: " + self.dataset[self.starting_index]['date_hour'])
                self.starting_condition_already_checked = True
                self.__add_event(id_stazione, self.dataset[self.starting_index]['date_hour'], self.EVENT_MATURAZIONE_OOSPORE)
                #sample['starting_condition_time'] = self.dataset[self.starting_index]['date_hour']
            if not self.phenologic_condition_already_checked:
                self.phenologic_condition_already_checked = True
                if self.phenologic_condition_index is not None:
                    print("Sviluppo fenologico vite rilevato (calcolato): " + self.dataset[self.phenologic_condition_index]['date_hour'])
                    self.__add_event(id_stazione, self.dataset[self.phenologic_condition_index]['date_hour'], self.EVENT_SVILUPPO_FENOLOGICO_VITE)
                if self.phenologic_condition_index_manual is not None:
                    print("Sviluppo fenologico vite (manuale): " + self.dataset[self.phenologic_condition_index_manual]['date_hour'])
                    self.__add_event(id_stazione, self.dataset[self.phenologic_condition_index_manual]['date_hour'], self.EVENT_SVILUPPO_FENOLOGICO_VITE, is_manual_model=True)
                #sample['phenologic_condition_time'] = self.dataset[self.phenologic_condition_index]['date_hour']
            if not self.is_update:
                if self.phenologic_condition_index_manual is not None:
                    self.phenologic_condition_index = self.phenologic_condition_index_manual
                    self.is_manual_model = True
                index = (self.starting_index if self.starting_index >= self.phenologic_condition_index else self.phenologic_condition_index) + 1
            else:
                index = 0
            is_infestation_present, primary_infestation_indexes, is_first_incubation_completed, first_incubation_completed_index = self.__check_primary_infestation(index)
            if is_infestation_present:
                #for i in range(0, len(primary_infestation_indexes)):
                #print "Infezione primaria rilevata: " + self.dataset[primary_infestation_indexes[i]]['date_hour']
                #sample['infestation_time'] = self.dataset[self.primary_infestation_index]['date_hour']
                #sample['incubation_percentage'] = self.incubation_percentage_sum
                if is_first_incubation_completed:
                    is_secondary_infestation_present, secondary_infestation_indexes = self.__check_secondary_infestation(first_incubation_completed_index + 1)
                    #if is_secondary_infestation_present:
                    #for i in range(0, len(secondary_infestation_indexes)):
                    #print "Infezione secondaria rilevata: " + self.dataset[secondary_infestation_indexes[i]]['date_hour']
            else:
                print("Infezione primaria NON rilevata")
        else:
            if self.starting_index is None and self.phenologic_condition_index is None:
                print("Maturazione oospore e sviluppo fenologico vite NON rilevato")
            elif self.starting_index is None:
                print("Sviluppo fenologico vite rilevato: " + self.dataset[self.phenologic_condition_index]['date_hour'])
                self.phenologic_condition_already_checked = True
                #sample['phenologic_condition_time'] = self.dataset[self.phenologic_condition_index]['date_hour']
                print("Maturazione oospore NON rilevata")
            elif self.phenologic_condition_index is None:
                print("Maturazione oospore rilevata: " + self.dataset[self.starting_index]['date_hour'])
                self.starting_condition_already_checked = True
                #sample['starting_condition_time'] = self.dataset[self.starting_index]['date_hour']
                print("Sviluppo fenologico vite NON rilevato")
        #else:
        #    print("Missing data")
        return self.output

    def evaluate_daily_leaf_wet(self):
        #if not self.check_missing_data():
        id_stazione = self.dataset[0]['id_stazione']
        index = 0
        print("Valutazione bagnatura fogliare giornaliera stazione: " + str(id_stazione))
        while index < len(self.dataset):
            daily_leaf_wet_hours = 0
            end_day = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).date()
            while datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).date() <= end_day and index < len(self.dataset) - 1:
                if self.__evaluate_leaf_wet(index) > 0:
                    daily_leaf_wet_hours = daily_leaf_wet_hours + 1
                index = index + 1
            #print(self.dataset[index]['date_hour'] + " valore: " + str(daily_leaf_wet_hours))
            self.__add_event(id_stazione, self.dataset[index]['date_hour'], self.EVENT_BAGNATURA_FOGLIARE, daily_leaf_wet_hours)
            index = index + 1
        #else:
        #    print("Missing data")

    def evaluate_day_degrees(self):
        id_stazione = self.dataset[0]['id_stazione']
        index = 0
        print("Valutazione gradi giorno stazione: " + str(id_stazione))
        day_degrees_tot = 0
        while index < len(self.dataset) - 1:
            current_day = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).day
            current_day_index = 0
            current_day_degrees = 0
            while datetime.strptime(self.dataset[index]['date_hour'],self.DATE_PATTERN).day == current_day and index < len(self.dataset) - 1:
                t_avg = self.get_t_avg_value(index)
                current_day_degrees += t_avg
                index += 1
                current_day_index += 1
            daily_temperature_avg = current_day_degrees / current_day_index
            if (daily_temperature_avg - self.PHENOLOGIC_STATE_ZERO_DEVELOP) > 0:
                day_degrees_tot += daily_temperature_avg - self.PHENOLOGIC_STATE_ZERO_DEVELOP
            self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'], self.EVENT_GRADI_GIORNO, day_degrees_tot)
            #print(self.dataset[index]['date_hour'] + " " + str(day_degrees_tot) + " " + str(daily_temperature_avg))

    def evaluate_phenologic_phases(self):
        #if not self.check_missing_data():
        id_stazione = self.dataset[0]['id_stazione']
        print("Valutazione fasi fenologiche stazione: " + str(id_stazione))
        index = self.__check_day_degrees_threshold(self.GERMOGLIAMENTO_THRESHOLD, self.PHENOLOGIC_STATE_ZERO_DEVELOP)
        if index is not None:
            self.__add_event(id_stazione, self.dataset[index]['date_hour'], self.EVENT_GERMOGLIAMENTO)
        index = self.__check_day_degrees_threshold(self.FIORITURA_THRESHOLD, self.PHENOLOGIC_STATE_ZERO_DEVELOP)
        if index is not None:
            self.__add_event(id_stazione, self.dataset[index]['date_hour'], self.EVENT_FIORITURA)
        index = self.__check_day_degrees_threshold(self.ALLEGAGIONE_THRESHOLD, self.PHENOLOGIC_STATE_ZERO_DEVELOP)
        if index is not None:
            self.__add_event(id_stazione, self.dataset[index]['date_hour'], self.EVENT_ALLEGAGIONE)
        index = self.__check_day_degrees_threshold(self.INIZIO_INVAIATURA_THRESHOLD, self.INIZIO_INVAIATURA_THRESHOLD)
        if index is not None:
            self.__add_event(id_stazione, self.dataset[index]['date_hour'], self.EVENT_INIZIO_INVAIATURA)
        index = self.__check_day_degrees_threshold(self.INVAIATURA_COMPLETA_tHRESHOLD, self.PHENOLOGIC_STATE_ZERO_DEVELOP)
        if index is not None:
            self.__add_event(id_stazione, self.dataset[index]['date_hour'], self.EVENT_INVAIATURA_COMPLETA)
        index = self.__check_day_degrees_threshold(self.MATURAZIONE_THRESHOLD, self.PHENOLOGIC_STATE_ZERO_DEVELOP)
        if index is not None:
            self.__add_event(id_stazione, self.dataset[index]['date_hour'], self.EVENT_MATURAZIONE)
        #else:
        #    print("Missing data")

    def __add_event(self, id_stazione, time, event, intensity=None, is_manual_model = False):
        sample = {'id_stazione': id_stazione, 'time': time, 'event': event}
        if intensity is not None:
            sample['intensity'] = intensity
        event_datetime = datetime.strptime(time, self.DATE_PATTERN)
        current_datetime = datetime.now()
        if event_datetime > current_datetime:
            sample['is_forecast'] = True
        else:
            sample['is_forecast'] = False
        sample['is_manual_model'] = is_manual_model
        self.output['data'].append(sample)

    def __update_sample(self,
                        data,
                        id_stazione,
                        lw_threshold,
                        starting_condition_time,
                        phenologic_condition_time=None,
                        germination_time=None,
                        infestation_time=None,
                        secondary_infestation_time=None,
                        secondary_infestation_number=None
                        ):
        sample = {'id_stazione': id_stazione, 'lw_threshold': lw_threshold, 'starting_condition_time': starting_condition_time}
        if phenologic_condition_time is not None:
            sample['phenologic_condition_time'] = phenologic_condition_time
        if germination_time is not None:
            sample['germination_time'] = germination_time
        if infestation_time is not None:
            sample['infestation_time'] = infestation_time
        if secondary_infestation_time is not None:
            sample['secondary_infestation_time'] = secondary_infestation_time
        if secondary_infestation_number is not None:
            sample['secondary_infestation_number'] = secondary_infestation_number
        data['data'].append(sample)

    def __check_day_degrees_threshold(self, threshold, zero_develop, index=0):
        #print("---- Check ----  zero: " + str(zero_develop) + " threshold: " + str(threshold))
        day_degrees_reached = False
        day_degrees_tot = 0
        while not day_degrees_reached and index < len(self.dataset) - 1:
            current_day = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).day
            current_day_index = 0
            current_day_degrees = 0
            while datetime.strptime(self.dataset[index]['date_hour'],self.DATE_PATTERN).day == current_day and index < len(self.dataset) - 1:
                t_avg = self.get_t_avg_value(index)
                current_day_degrees += t_avg
                index += 1
                current_day_index += 1
            daily_temperature_avg = current_day_degrees / current_day_index
            if (daily_temperature_avg - zero_develop) > 0:
                day_degrees_tot += daily_temperature_avg - zero_develop
            #print(self.dataset[index]['date_hour'] + " " + str(day_degrees_tot) + " " + str(daily_temperature_avg))
            if day_degrees_tot >= threshold:
                day_degrees_reached = True
        if day_degrees_reached:
            return index - 1
        else:
            return None

    def __check_starting_condition(self):
        return self.__check_day_degrees_threshold(self.STARTING_CONDITION_DAY_DEGREES_THRESHOLD, self.STARTING_CONDITION_ZERO_DEVELOP)

    def __check_phenologic_condition(self):
        return self.__check_day_degrees_threshold(self.PHENOLOGIC_STATE_DAY_DEGREES_THRESHOLD, self.PHENOLOGIC_STATE_ZERO_DEVELOP)

    def __check_germination_phase(self, index):
        is_germination_succeeded = False
        germination_index = None
        temperature_condition_met = False
        while not is_germination_succeeded and index < len(self.dataset):
            rain_mm_sum = 0
            i = 0
            date_start = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN) - timedelta(hours=48)
            while datetime.strptime(self.dataset[index - i]['date_hour'], self.DATE_PATTERN) > date_start and (index - i) > 0:
                t_avg = self.get_t_avg_value(index)
                if t_avg > self.GERMINATION_TEMPERATURE_AVG_THRESHOLD:
                    temperature_condition_met = True
                rain_mm_sum += self.get_rain_tot_value(index - 1)
                i += 1
            #rain_mm_avg_hour = rain_mm_sum / (i - 1)
            #print "GERMINATION CHECK: " + self.dataset[index]['date_hour'] + " rain sum: " + str(rain_mm_sum) + " temp condition met: " + str(temperature_condition_met)
            if temperature_condition_met and rain_mm_sum >= self.GERMINATION_RAIN_MM_SUM_THRESHOLD:
                is_germination_succeeded = True
                germination_index = index
            index += 1
        return is_germination_succeeded, germination_index

    def __check_primary_infestation(self, index):
        is_infestation_present = False
        is_first_incubation_completed = False
        first_incubation_completed_index = None
        primary_infestation_indexes = []
        current_date = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).date()
        end_date = datetime.strptime(self.dataset[len(self.dataset) - 1]['date_hour'], self.DATE_PATTERN).date()
        while current_date < end_date:
            is_germination_succeeded, germination_index = self.__check_germination_phase(index)
            if is_germination_succeeded and germination_index is not None:
                index = germination_index + 1
                date_end = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN) + timedelta(hours=6)
                day_degrees_lw_sum = 0
                rain_intensity_max = 0
                while datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN) < date_end and index < len(self.dataset) - 1:
                    is_infestation_present = True
                    t_avg = self.get_t_avg_value(index)
                    day_degrees_lw = self.__evaluate_leaf_wet(index) * t_avg
                    day_degrees_lw_sum += day_degrees_lw
                    rain_intensity = self.get_rain_max_value(index) * 4
                    if rain_intensity_max < rain_intensity:
                        rain_intensity_max = rain_intensity
                    #print(self.dataset[index]['date_hour'] + " day_degrees: " + str(day_degrees_lw) + " day_degrees_sum: " + str(day_degrees_lw_sum) + " rain_max: " + str(self.dataset[index]['rain_max']) + " rain_intensity: " + str(rain_intensity) + " rain_intensity_max:" + str(rain_intensity_max))
                    index += 1
                if day_degrees_lw_sum >= self.PRIMARY_INFESTATION_DAY_DEGREES_THRESHOLD and rain_intensity_max > self.PRIMARY_INFESTATION_RAIN_INTENSITY_THRESHOLD:
                    primary_infestation_indexes.append(index)
                    self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'], self.EVENT_INFEZIONE_PRIMARIA, self.__evaluate_infestation_intensity(index),is_manual_model=self.is_manual_model)
                    is_incubation_threshold_reached, incubation_threshold_reached_index, is_incubation_completed, incubation_completed_index, incubation_percentage_sum = self.__calculate_incubation(index, True)
                    if not is_first_incubation_completed and is_incubation_completed:
                        is_first_incubation_completed = True
                        first_incubation_completed_index = incubation_completed_index
                self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'], self.EVENT_GRADI_BAGNATURA, day_degrees_lw_sum,is_manual_model=self.is_manual_model)
            else:
                index += 1
            current_date = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).date()
        #return is_infestation_present, primary_infestation_indexes, incubation_percentage_sum, is_incubation_reached, primary_infestation_incubation_index
        return is_infestation_present, primary_infestation_indexes, is_first_incubation_completed, first_incubation_completed_index

    def __check_secondary_infestation(self, index):
        secondary_infestation_indexes = []
        is_secondary_infestation_found = False
        updated_sporulation_end_date = None
        day_degrees_lw_sum = 0
        while index < len(self.dataset):
            is_sporulation_found, sporulation_index, sporulation_end_date = self.__check_sporulation(index)
            curr_date = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).date()
            if sporulation_end_date is not None:
                updated_sporulation_end_date = sporulation_end_date
            t_avg = self.get_t_avg_value(index)
            if t_avg < 3 and t_avg > 29:
                t_avg = 0
            day_degrees_lw = self.__evaluate_leaf_wet(index) * t_avg
            if day_degrees_lw != 0:
                day_degrees_lw_sum += day_degrees_lw
            else:
                day_degrees_lw_sum = 0
            #print "SECONDARY CHECK " + self.dataset[index]['date_hour'] + " day_degrees: " + str(day_degrees_lw) + " day_degrees_sum: " + str(day_degrees_lw_sum)
            if updated_sporulation_end_date is not None:
                if curr_date < updated_sporulation_end_date and day_degrees_lw_sum > self.SEC_INFESTATION_DAY_DEGREES_THRESHOLD:
                    is_secondary_infestation_found = True
                    secondary_infestation_indexes.append(index)
                    self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'], self.EVENT_INFEZIONE_SECONDARIA, self.__evaluate_infestation_intensity(index),is_manual_model=self.is_manual_model)
                    #print "SECONDARY CHECK " + self.dataset[index]['date_hour'] + "INFESTATION_FOUND day_degrees: " + str(day_degrees_lw) + " day_degrees_sum: " + str(day_degrees_lw_sum)
                    is_incubation_threshold_reached, incubation_threshold_reached_index, is_incubation_completed, incubation_completed_index, incubation_percentage_sum = self.__calculate_incubation(index, False)
            index += 1
        return is_secondary_infestation_found, secondary_infestation_indexes

    def __calculate_incubation(self, index, is_primary):
        is_incubation_threshold_reached = False
        is_incubation_completed = False
        incubation_threshold_reached_index = None
        is_incubation_completed_index = None
        incubation_percentage_sum = 0
        while not is_incubation_completed and index < len(self.dataset) - 1:
            current_day = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).day
            current_day_index = 0
            current_day_degrees = 0
            while datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).day == current_day and index < len(self.dataset) - 1:
                t_avg = self.get_t_avg_value(index)
                current_day_degrees += t_avg
                index += 1
                current_day_index += 1
            incubation_percentage_sum += self.get_muller_value(current_day_degrees / current_day_index)
            if incubation_percentage_sum >= 100:
                is_incubation_completed = True
                is_incubation_completed_index = index
            elif incubation_percentage_sum >= self.INCUBATION_THRESHOLD_PERCENTAGE:
                is_incubation_threshold_reached = True
                incubation_threshold_reached_index = index
                if is_primary:
                    self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'],self.EVENT_SOGLIA_INCUBAZIONE_PRIMARIA_SUPERATA, incubation_percentage_sum,is_manual_model=self.is_manual_model)
                else:
                    self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'],self.EVENT_SOGLIA_INCUBAZIONE_SECONDARIA_SUPERATA, incubation_percentage_sum, is_manual_model=self.is_manual_model)
            else:
                if is_primary:
                    self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'],self.EVENT_INCUBAZIONE_PRIMARIA, incubation_percentage_sum, is_manual_model=self.is_manual_model)
                else:
                    self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'],self.EVENT_INCUBAZIONE_SECONDARIA, incubation_percentage_sum, is_manual_model=self.is_manual_model)
        return is_incubation_threshold_reached, incubation_threshold_reached_index, is_incubation_completed, is_incubation_completed_index, incubation_percentage_sum

    def __evaluate_leaf_wet(self, index):
        lw_array_top, lw_array_bot = self.get_lw_values(index)
        lw_minutes = 0
        lw_array_length = min(len(lw_array_top), len(lw_array_bot))
        for i in range(0, lw_array_length):
            lw_avg = (lw_array_top[i] + lw_array_bot[i])/2
            if lw_avg > self.PRIMARY_INFESTATION_LW_THRESHOLD_PERCENTAGE:
                lw_minutes += 1
        if lw_minutes > self.LW_THRESHOLD_MINUTES:
            lw = float(lw_minutes) / float(lw_array_length)
        else:
            lw = 0
        return lw

    def __evaluate_infestation_intensity(self, index):
        day_degrees_lw_sum_max_array = []
        intensity = 0
        end_day = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).date()
        index = self.__get_start_day_index(index)
        if self.__evaluate_leaf_wet(index) != 0:
            index = self.__get_start_day_index(index - 1)
        while datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).date() <= end_day and index < len(self.dataset) - 1:
            #sample = self.dataset[index]
            day_degrees_lw_sum = 0
            day_degrees_lw_sum_max = 0
            current_day = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).day
            while datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).day == current_day and index < len(self.dataset) - 1:
                lw_percentage = self.__evaluate_leaf_wet(index)
                if lw_percentage != 0:
                    t_avg = self.get_t_avg_value(index)
                    day_degrees =  lw_percentage * t_avg
                    day_degrees_lw_sum += day_degrees
                    if day_degrees_lw_sum > day_degrees_lw_sum_max:
                        day_degrees_lw_sum_max = day_degrees_lw_sum
                else:
                    day_degrees_lw_sum = 0
                index += 1
            day_degrees_lw_sum_max_array.append(day_degrees_lw_sum_max)
        for i in range(0, len(day_degrees_lw_sum_max_array)):
            intensity += day_degrees_lw_sum_max_array[i]
        #print "INSENSITY CHECK: " + self.dataset[index]['date_hour'] + " intensity: " + str(intensity)
        return intensity

    def __get_start_day_index(self, index):
        while datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).day == datetime.strptime(self.dataset[index - 1]['date_hour'], self.DATE_PATTERN).day:
            index -= 1
        return index

    def __get_end_day_index(self, index):
        while datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).day == datetime.strptime(self.dataset[index + 1]['date_hour'], self.DATE_PATTERN).day:
            index += 1
        return index

    def __get_index_by_date(self, datestring):
        index = 0
        date = datetime.strptime(datestring, self.DATE_PATTERN)
        current_date = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN)
        if date > datetime.strptime(self.dataset[len(self.dataset) - 1]['date_hour'], self.DATE_PATTERN):
            return None
        else:
            while datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN) < date and index < len(self.dataset) - 1:
                index += 1
        return index

    # OLD SPORULATION
    # def __check_sporulation(self, index):
    #     is_sporulation_found = False
    #     sporulation_index = None
    #     sporulation_end_date = None
    #     while not is_sporulation_found and index < len(self.dataset):
    #         consecutive_hour_infestation_met = 0
    #         start_hour, end_hour = self.__get_night_hours_by_date(self.dataset[index]['date_hour'])
    #         while not is_sporulation_found and (datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).hour <= end_hour
    #                                                       or datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).hour >= start_hour) and index < len(self.dataset) - 1:
    #             #print "SPORULATION CHECK: " + self.dataset[index]['date_hour'] + " t_min: " + str(self.dataset[index]['t_min']) + " hum_min: " + str(self.dataset[index]['hum_min'])
    #             if self.dataset[index]['t_min'] >= self.SPORULATION_TEMPERATURE_THRESHOLD and self.dataset[index]['hum_min'] >= self.SPORULATION_HUMIDITY_THRESHOLD:
    #                 consecutive_hour_infestation_met += 1
    #                 if consecutive_hour_infestation_met >= self.SPORULATION_CONSECUTIVE_HOURS:
    #                     is_sporulation_found = True
    #                     sporulation_index = index
    #                     sporulation_end_date = (datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN) +  timedelta(days=self.SPORULATION_PERSISTENCE_DAYS)).date()
    #                     self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'], self.EVENT_SPORULAZIONE)
    #             index += 1
    #         index += 1
    #     return is_sporulation_found, sporulation_index, sporulation_end_date

    def __check_sporulation(self, index):
        is_sporulation_found = True
        starting_index = index
        sporulation_index = None
        sporulation_end_date = None
        consecutive_hour_sporulation_met = 0
        date_start = datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN) - timedelta(hours=self.SPORULATION_CONSECUTIVE_HOURS)
        #print "SPORULATION CHECK: " + self.dataset[index]['date_hour'] + " t_min: " + str(self.dataset[index]['t_min']) + " hum_min: " + str(self.dataset[index]['hum_min'])
        while is_sporulation_found and datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN) >= date_start:
            start_hour, end_hour = self.__get_night_hours_by_date(self.dataset[index]['date_hour'])
            if (datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).hour > end_hour and datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN).hour < start_hour)\
                    or self.get_t_min_value(index) < self.SPORULATION_TEMPERATURE_THRESHOLD or self.get_hum_min_value(index) < self.SPORULATION_HUMIDITY_THRESHOLD:
                is_sporulation_found = False
                #print "SPORULATION FAIL: " + self.dataset[index]['date_hour'] + " t_min: " + str(self.dataset[index]['t_min']) + " hum_min: " + str(self.dataset[index]['hum_min'])
            else:
                #print "SPORULATION FOUND: " + self.dataset[index]['date_hour'] + " t_min: " + str(self.dataset[index]['t_min']) + " hum_min: " + str(self.dataset[index]['hum_min'])
                consecutive_hour_sporulation_met += 1
            index -= 1
        if is_sporulation_found and consecutive_hour_sporulation_met >= self.SPORULATION_CONSECUTIVE_HOURS:
            sporulation_index = starting_index
            sporulation_end_date = (datetime.strptime(self.dataset[index]['date_hour'], self.DATE_PATTERN) + timedelta(days=self.SPORULATION_PERSISTENCE_DAYS)).date()
            self.__add_event(self.dataset[0]['id_stazione'], self.dataset[index]['date_hour'], self.EVENT_SPORULAZIONE, is_manual_model=self.is_manual_model)
        return is_sporulation_found, sporulation_index, sporulation_end_date

    def __get_night_hours_by_date(self, date_string):
        year = 2000
        date = datetime.strptime(date_string, self.DATE_PATTERN)
        current_date = date_time.date(year, date.month, date.day)
        if date_time.date(year, 5, 1) <= current_date <= date_time.date(year, 5, 15):
            start_hour = 21
            end_hour = 5
        elif date_time.date(year, 5, 16) <= current_date <= date_time.date(year, 8, 8):
            start_hour = 22
            end_hour = 5
        elif date_time.date(year, 8, 9) <= current_date <= date_time.date(year, 8, 25):
            start_hour = 21
            end_hour = 5
        elif date_time.date(year, 8, 26) <= current_date <= date_time.date(year, 9, 12):
            start_hour = 21
            end_hour = 6
        elif date_time.date(year, 9, 13) <= current_date <= date_time.date(year, 10, 18):
            start_hour = 20
            end_hour = 6
        elif date_time.date(year, 10, 19) <= current_date <= date_time.date(year, 10, 31):
            start_hour = 19
            end_hour = 7
        else:
            start_hour = 21
            end_hour = 5
        return start_hour, end_hour

    @staticmethod
    def get_muller_value(daily_temperature):
        if 5 <= daily_temperature < 6:
            incubation_value = 4
        elif 6 <= daily_temperature < 7:
            incubation_value = 4
        elif 7 <= daily_temperature < 8:
            incubation_value = 5
        elif 8 <= daily_temperature < 9:
            incubation_value = 5
        elif 9 <= daily_temperature < 10:
            incubation_value = 6
        elif 10 <= daily_temperature < 11:
            incubation_value = 6
        elif 11 <= daily_temperature < 12:
            incubation_value = 7
        elif 12 <= daily_temperature < 13:
            incubation_value = 8
        elif 13 <= daily_temperature < 14:
            incubation_value = 9
        elif 14 <= daily_temperature < 15:
            incubation_value = 10
        elif 15 <= daily_temperature < 16:
            incubation_value = 11
        elif 16 <= daily_temperature < 17:
            incubation_value = 13
        elif 17 <= daily_temperature < 18:
            incubation_value = 14
        elif 18 <= daily_temperature < 19:
            incubation_value = 16
        elif 19 <= daily_temperature < 20:
            incubation_value = 18
        elif 20 <= daily_temperature < 21:
            incubation_value = 20
        elif 21 <= daily_temperature < 22:
            incubation_value = 21
        elif 22 <= daily_temperature < 23:
            incubation_value = 24
        elif 23 <= daily_temperature < 24:
            incubation_value = 25
        elif 24 <= daily_temperature < 25:
            incubation_value = 25
        elif 25 <= daily_temperature < 26:
            incubation_value = 26
        elif 26 <= daily_temperature < 27:
            incubation_value = 25
        elif daily_temperature > 27:
            incubation_value = 14
        elif daily_temperature < 5:
            incubation_value = 2
        return incubation_value